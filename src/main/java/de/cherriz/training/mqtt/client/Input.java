package de.cherriz.training.mqtt.client;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.Scanner;

public class Input extends Thread {

    private Scanner in;

    private MqttClient client;

    private String topic;

    private String user;

    public Input(Scanner in, MqttClient client, String topic, String user){
        this.in = in;
        this.client = client;
        this.topic = topic;
        this.user = user;
    }

    @Override
    public void run() {
        while (in.hasNext()){
            String input = in.nextLine();
            MqttMessage message = new MqttMessage();
            message.setPayload((user+": "+input).getBytes());
            try {
                client.publish(topic, message);
            } catch (MqttException e) {
                System.err.println("Konnte Nachricht nicht senden.");
                e.printStackTrace();
            }
        }
    }

}
