package de.cherriz.training.mqtt.client;

import org.eclipse.paho.client.mqttv3.*;

public class Output extends Thread implements MqttCallback {

    private MqttClient client;

    private String topic;

    private String user;

    public Output(MqttClient client, String topic, String user) {
        this.client = client;
        this.topic = topic;
        this.user = user;
    }

    @Override
    public void run() {
        client.setCallback(this);
        try {
            client.subscribe(topic);
            System.out.println("Chatraum " + topic + " betreten.");
        } catch (MqttException e) {
            System.err.println("Konnte Topic nicht abonnieren.");
            e.printStackTrace();
        }
    }

    @Override
    public void connectionLost(Throwable throwable) {
        System.err.println("Verbindung verloren.");
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) {
        if(!message.toString().startsWith(user)){
            System.out.println(message.toString());
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }
}
