package de.cherriz.training.mqtt.client;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.util.Scanner;

public class Starter {

    private String user;

    private String broker;

    private String clientId;

    private MemoryPersistence persistence = new MemoryPersistence();

    private String topic = "/cherriz/training/mqtt/chat";

    private MqttClient client;

    private Scanner sc;

    public static void main(String[] args) throws MqttException {
        Starter starter = new Starter();
        starter.initCfg();
        starter.connect();
        starter.startReceive();
        starter.startSend();
    }

    private void initCfg() {
        System.out.println("#".repeat(41));
        System.out.println("## Cherriz Training MQTT - Chat Client ##");
        System.out.println("#".repeat(41));

        sc = new Scanner(System.in);
        System.out.println("\nBitte Adresse zum MQTT Broker eingeben (Default localhost):");
        String url = sc.nextLine();
        if (url.isBlank()) {
            url = "127.0.0.1";
        }

        System.out.println("Bitte Port zum MQTT Broker eingeben (Default 1883):");
        String port = sc.nextLine();
        if (port.isBlank()) {
            port = "1883";
        }

        System.out.println("Bitte gib deinen Benutzernamen ein:");
        user = sc.next();

        broker = "tcp://" + url + ":" + port;
        clientId = "MQTTChat" + user + "_" + System.currentTimeMillis();
    }

    private void connect() throws MqttException {
        client = new MqttClient(broker, clientId, persistence);
        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setCleanSession(true);
        System.out.println("Verbinde mit Broker: " + broker);
        client.connect(connOpts);
    }

    private void startSend() {
        new Input(sc, client, topic, user).run();
    }

    private void startReceive() {
        new Output(client, topic, user).run();
    }

}
